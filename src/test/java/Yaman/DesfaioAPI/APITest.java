package Yaman.DesfaioAPI;

import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;

public class APITest {
	@Test
	public void whenRequestGet_thenOK() {
		String name = when().get("https://api.trello.com/1/actions/592f11060f95a3d3d46a987a").then().statusCode(200)
				.and().extract().path("data.list.name").toString();
		System.out.println(name);
	}
}
